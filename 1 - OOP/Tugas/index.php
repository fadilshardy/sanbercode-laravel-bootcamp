<?php
require 'Elang.php';
require 'Harimau.php';

$elang = new Elang('Elang');
$harimau = new Harimau('Harimau');

$elang->getInfoHewan();
echo "<br>";

$elang->atraksi();
echo "<br>";

$elang->serang($harimau);
echo "<br> <br>";

$harimau->getInfoHewan();
echo "<br>";

$harimau->atraksi();
echo "<br>";

$harimau->serang($elang);
