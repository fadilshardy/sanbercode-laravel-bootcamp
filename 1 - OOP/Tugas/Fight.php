<?php

trait Fight
{
    public $attackPower, $defencePower;

    public function serang($lawan)
    {
        echo "{$this->nama} sedang menyerang {$lawan->nama} <br>";
        return $this->diserang($lawan);
    }

    private function diserang($lawan)
    {
        echo "{$lawan->nama} sedang diserang {$this->nama} <br>";
        echo "Sisa Darah {$lawan->nama} : " . ($this->darah - $this->attackPower / $lawan->defencePower);
    }
}