<<?php
require_once 'Hewan.php';
require_once 'Fight.php';

class Elang
{
    use Hewan, Fight;

    public function __construct($nama, $jumlahKaki = 2, $keahlian = 'terbang tinggi', $attackPower = 10, $defencePower = 5)
    {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    public function getInfoHewan()
    {
        echo "Jenis Hewan : {$this->nama} <br>";
        echo "Darah : {$this->darah} <br>";
        echo "Jumlah Kaki : {$this->jumlahKaki} <br>";
        echo "Keahlian : {$this->keahlian} <br>";
        echo "Attack Power : {$this->attackPower} <br>";
        echo "Defence Power : {$this->defencePower} <br>";
    }
}
